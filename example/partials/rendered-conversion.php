<?php
/** @var \OliverNoth\ArabicRomanNumberConverter\Converter\ArabicRomanNumberConverter $numberConverter */

/** @var string $convertedNumeral */

/** @var array $conversionNotes */

// Renew csrf token
$_SESSION['_csrf_token'] = base64_encode(random_bytes(32));
?>
<div>
    <?php /* Replaces existing hidden input initialized in header.php (see also assets/js/example[.min].js) */ ?>
    <input type="hidden" name="_csrf" value="<?= $_SESSION['_csrf_token'] ?>">

    <p class="lead">Conversion results for: '<?= htmlentities($numberConverter->getCandidate()) ?>'</p>
    <div class="text-white bg-info mt-0">
        <div id="accordianId" class="accordion bg-info" role="tablist" aria-multiselectable="true">
            <div class="card rounded-0 bg-white text-info">
                <div class="card-header bg-white text-info" role="tab" id="HeaderId">
                    <p class="mb-0">
                        <a class="btn btn-block btn-lg bg-info text-white text-left"
                           data-toggle="collapse" data-parent="#accordianId" href="#ContentId"
                           aria-expanded="true" aria-controls="ContentId">
                             <?= count($conversionNotes) ?> Conversion result<?= 1 < count($conversionNotes) ? 's' : '' ?>
                             <i class="fa fa-chevron-circle-down pull-right" aria-hidden="true"></i>
                             <i class="fa fa-chevron-circle-up pull-right" aria-hidden="true"></i>
                        </a>
                    </p>
                </div>
                <div id="ContentId" class="collapse show" role="tabpanel" aria-labelledby="HeaderId" data-parent="#accordianId">
                    <div class="card-body pl-5 pr-5">
                        <div class="row">
                            <?php foreach ($conversionNotes as $note): ?>
                                <div class="col-12 text-left">
                                    <?= $note ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
