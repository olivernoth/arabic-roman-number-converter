<?php
// Create csrf token (will be posted on form submit, see hidden input below)
$_SESSION['_csrf_token'] = base64_encode(random_bytes(32));
?>

<header>
    <div class="jumbotron rounded-0 bg-info text-white my-n3">
        <div class="container">
            <h1 class="display-3" role="heading">ArabicRomanNumberConverter</h1>
            <form id="get-converted-numeral-from" class="needs-validation" novalidate>
                <input type="hidden" name="_csrf" value="<?= $_SESSION['_csrf_token'] ?>">
                <div class="form-row">
                    <label for="numeral-input" class="d-none">Arabic or Roman numeral to be converted</label>
                    <div class="form-group col-md-11 col-9 pr-0">
                        <input id="numeral-input" name="numeral-input" type="text" class="form-control border-0" placeholder="Enter arabic or roman numeral..." autocomplete="off">
                        <div class="invalid-tooltip">Please provide a valid numeral</div>
                    </div>
                    <div class="form-group col-md-1 col-3 pl-0 rounded-right bg-white text-right">
                        <button type="submit" class="border-0 btn bg-white text-info">
                            <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                            <span class="sr-only">Loading...</span>
                            <span class="fa-icon">
                                <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                            </span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</header>
