<?php
declare(strict_types=1);
/**
 * Main class
 *   - Converts an arabic or roman numeral to the opposite numeral
 *   - Use class-methods suggested by IDE-Code-Completion
 *
 * @class ArabicRomanNumberConverter
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2021 Oliver Noth
 */

namespace OliverNoth\ArabicRomanNumberConverter\Converter;

/**
 * Class ArabicRomanNumberConverter
 *
 * @package OliverNoth\ArabicRomanNumberConverter
 * @since 1.0.0
 */
class ArabicRomanNumberConverter
{
    /**
     * Constants used to set the currently determined type.
     *
     * @var ?string
     * @see ArabicRomanNumberConverter::determineType()
     * @since 1.0.0
     */
    protected const TYPE_ROMAN = 'roman';
    protected const TYPE_ARABIC = 'arabic';

    /**
     * Holds type determined in $this->determineType().
     *
     * @var ?string
     * @see ArabicRomanNumberConverter::determineType()
     * @since 1.0.0
     */
    protected ?string $type = null;

    /**
     * The candidate (an arabic or roman numeral) to be converted.
     *
     * Can be set either in $this->__construct or $this->convert or $this->setCandidate.
     *
     * @var ?string
     * @see ArabicRomanNumberConverter::__construct()
     * @see ArabicRomanNumberConverter::convert()
     * @see ArabicRomanNumberConverter::setCandidate()
     * @see ArabicRomanNumberConverter::getCandidate()
     * @since 1.0.0
     */
    protected ?string $candidate = null;

    /**
     * Holds conversion notes.
     *
     * Can be set either in $this->convert or $this->validate.
     *
     * @var array
     * @see ArabicRomanNumberConverter::convert()
     * @see ArabicRomanNumberConverter::validate()
     * @since 1.0.0
     */
    protected array $conversionNotes = [];

    /**
     * Constructor
     *
     * @param ?string $candidate
     * @since 1.0.0
     */
    public function __construct(?string $candidate = null)
    {
        $this->setCandidate($candidate);
    }

    /**
     * Getter for $this->candidate.
     *
     * @return ?string
     * @see ArabicRomanNumberConverter::$candidate
     * @since 1.0.0
     */
    public function getCandidate(): ?string
    {
        return $this->candidate;
    }

    /**
     * Setter for $this->candidate.
     *
     * @param ?string $candidate
     * @return ArabicRomanNumberConverter
     * @see ArabicRomanNumberConverter::$candidate
     * @since 1.0.0
     */
    public function setCandidate(?string $candidate): self
    {
        $this->candidate = $candidate;
        return $this;
    }

    /**
     * Getter for $this->conversionNotes.
     *
     * @return array
     * @see ArabicRomanNumberConverter::$conversionNotes
     * @since 1.0.0
     */
    public function getConversionNotes(): array
    {
        return $this->conversionNotes;
    }

    /**
     * Converts a given $candidate to the corresponding numeral depending on determined type (arabic or roman numeral).
     * Furthermore, a conversion note will be saved in $this->conversionNotes.
     *
     * @param ?string $candidate The candidate to be converted
     * @return string|int|null
     * @see ArabicRomanNumberConverter::determineType()
     * @see ArabicRomanNumberConverter::convertRoman()
     * @see ArabicRomanNumberConverter::convertArabic()
     * @since 1.0.0
     */
    public function convert(?string $candidate = null): string|int|null
    {
        $candidate ??= $this->candidate;

        $parts = $this->determineType($candidate);

        $conversionNote = "%s numeral '%s' has been converted to %s numeral: '%s'.";

        switch ($this->type) {
            case static::TYPE_ROMAN:
                $arabic = $this->convertRoman($parts);
                $this->conversionNotes = array_unique(array_merge($this->conversionNotes, [sprintf($conversionNote, 'Roman', $candidate, 'Arabic', $arabic)]));
                return $arabic;
            case static::TYPE_ARABIC:
                $roman = $this->convertArabic((int) $candidate);
                $this->conversionNotes = array_unique(array_merge($this->conversionNotes, [sprintf($conversionNote, 'Arabic', $candidate, 'Roman', $roman)]));
                return $roman;
            default:
                $this->conversionNotes = array_unique(array_merge($this->conversionNotes, ["Given '{$candidate}' could not be converted."]));
                return $candidate;
        }
    }

    /**
     * Determines the type of given $candidate (arabic or roman numeral) by validating it.
     * Sets $this->type and returns validation results in an array.
     *
     * If the validation fails, no type will be set and null will be returned.
     * Called in $this->convert().
     *
     * Sample return for given $candidate = 'MCMLXVIII':
     * ```
     * [
     *     0 => 'MCM',
     *     1 => 'LX',
     *     2 => 'VIII',
     * ]
     * ```
     *
     * @param ?string $candidate The numeral to be validated
     * @return ?array Validation results in an array
     * @see ArabicRomanNumberConverter::validate()
     * @since 1.0.0
     */
    protected function determineType(?string $candidate): ?array
    {
        if ($validationResult = $this->validate($candidate)) {
            $this->type = array_key_first($validationResult);

            return $validationResult[$this->type];
        }

        return null;
    }

    /**
     * Validates given $candidate (arabic or roman numeral).
     * If the validation fails, validation note will be saved in $this->conversionNotes and null will be returned.
     * Called in $this->determineType.
     *
     * @param ?string $candidate The candidate to validate (an arabic or a roman numeral).
     * @return ?array Validation results in an array
     * @see ArabicRomanNumberConverter::determineType()
     * @see ArabicRomanNumberConverter::$conversionNotes
     * @since 1.0.0
     */
    public function validate(?string $candidate = null): ?array
    {
        $candidate ??= $this->candidate;

        try {
            return $this->validateRomanNumeral($candidate);
        } catch (\InvalidArgumentException $romanNumeralException) {
            $romanValidationNote = $romanNumeralException->getMessage();
        }

        try {
            return $this->validateArabicNumeral($candidate);
        } catch (\InvalidArgumentException $arabicNumeralException) {
            $arabicValidationNote = $arabicNumeralException->getMessage();
        }

        if ($romanValidationNote || $arabicValidationNote) {
            $this->conversionNotes = array_unique(array_merge($this->conversionNotes, [$romanValidationNote ?: $arabicValidationNote]));
        }

        return null;
    }

    /**
     * Validates given roman numeral.
     * If the validation fails, an InvalidArgumentException will be thrown.
     *
     * @param ?string $roman
     * @return ?array
     * @throws \InvalidArgumentException
     * @since 1.0.0
     */
    protected function validateRomanNumeral(?string $roman = null): ?array
    {
        $checked = is_string($roman) && strlen($roman);

        // Empty validation failed
        if (!$checked) {
            throw new \InvalidArgumentException("Failed! An empty string cannot be converted.");
        // Type validation failed
        } else if (!preg_match('/^([M]{0,}|[M]{0,}CM|[M]{0,}D[C]{0,3}|[M]{0,}CD|[M]{0,}C{0,3})([X]{0,3}|XC|L[X]{0,3}|XL)([I]{0,3}|IX|V[I]{0,3}|IV)$/', $roman, $matches)) {
            throw new \InvalidArgumentException(sprintf("Failed! Given '%s' is neither an arabic number nor a roman numeral.", $roman));
        }

        return [
            static::TYPE_ROMAN => array_slice($matches, 1),
        ];
    }

    /**
     * Validates given arabic numeral.
     * If the validation fails, an InvalidArgumentException will be thrown.
     *
     * @param ?string $arabic
     * @return ?array
     * @throws \InvalidArgumentException
     * @since 1.0.0
     */
    protected function validateArabicNumeral(?string $arabic = null): ?array
    {
        $checked = is_string($arabic) && strlen($arabic);

        // Empty validation failed
        if (!$checked) {
            throw new \InvalidArgumentException("Failed! An empty string cannot be converted.");
        // Type validation failed
        } else if (!preg_match('/^\d+$/', $arabic, $matches)) {
            throw new \InvalidArgumentException(sprintf("Failed! '%s' is neither an arabic number nor a roman numeral.", $arabic));
        }

        return [
            static::TYPE_ARABIC => $matches,
        ];
    }

    /**
     * Converts an arabic number to a roman numeral and returns it.
     * Called in $this->convert().
     *
     * @param int $arabic The arabic numeral to be converted into a roman one
     * @return string The conversion result (a roman numeral)
     * @see ArabicRomanNumberConverter::convert()
     * @since 1.0.0
     */
    protected function convertArabic(int $arabic): string
    {
        // Determine counts of roman digits
        $hundreds = (int) (($arabic % 1000) / 100);
        $tens = (int) (($arabic % 100) / 10);
        $ones = $arabic % 10;

        // Initialize an array of roman digits and their counts, from which the roman numeral finally is built of (see below)
        $romanDigits = array_filter([
            'M' => (int) ($arabic / 1000),
            'CM' => (int) (9 === $hundreds),
            'D' => (int) (5 <= $hundreds && 9 > $hundreds),
            'CD' => (int) (4 === $hundreds),
            'C' => 3 >= $hundreds % 5 ? $hundreds % 5 : 0,
            'XC' => (int) (9 === $tens),
            'L' => (int) (5 <= $tens && 9 > $tens),
            'XL' => (int) (4 === $tens),
            'X' => 3 >= $tens % 5 ? $tens % 5 : 0,
            'IX' => (int) (9 === $ones),
            'V' => (int) (5 <= $ones && 9 > $ones),
            'IV' => (int) (4 === $ones),
            'I' => 3 >= $ones % 5 ? $ones % 5 : 0,
        ]);

        // Finally, build the roman numeral and return it
        return implode('', array_map(function($digit, $count) {
            return str_repeat($digit, $count);
        }, array_keys($romanDigits), array_values($romanDigits)));
    }

    /**
     * Converts roman numeral parts to an arabic number and returns it.
     * Called in $this->convert().
     *
     * Sample for given $romanNumeralParts:
     * ```
     * [
     *     0 => 'MCM',
     *     1 => 'LX',
     *     2 => 'VIII',
     * ]
     * ```
     *
     * @param array $romanNumeralParts The roman numeral parts to calculate the arabic number from
     * @return int The conversion result (an arabic number)
     * @see ArabicRomanNumberConverter::convert()
     * @since 1.0.0
     */
    protected function convertRoman(array $romanNumeralParts): int
    {
        $hundreds = match (true) {
            str_contains($romanNumeralParts[0], 'CM') => substr_count(str_replace('CM', '', $romanNumeralParts[0]), 'M') * 1000 + 900,
            str_contains($romanNumeralParts[0], 'CD') => substr_count(str_replace('CD', '', $romanNumeralParts[0]), 'M') * 1000 + 400,
            str_contains($romanNumeralParts[0], 'D') => substr_count(str_replace('D', '', $romanNumeralParts[0]), 'M') * 1000 + 500 + substr_count(str_replace(['M', 'D'], '', $romanNumeralParts[0]), 'C') * 100,
            default => substr_count($romanNumeralParts[0], 'M') * 1000 + substr_count(str_replace('M', '', $romanNumeralParts[0]), 'C') * 100,
        };

        $tens = match (true) {
            str_contains($romanNumeralParts[1], 'XC') => 90,
            str_contains($romanNumeralParts[1], 'XL') => 40,
            str_contains($romanNumeralParts[1], 'L') => 50 + substr_count(str_replace('L', '', $romanNumeralParts[1]), 'X') * 10,
            default => substr_count($romanNumeralParts[1], 'X') * 10,
        };

        $ones = match (true) {
            str_contains($romanNumeralParts[2], 'IX') => 9,
            str_contains($romanNumeralParts[2], 'IV') => 4,
            str_contains($romanNumeralParts[2], 'V') => 5 + substr_count(str_replace('V', '', $romanNumeralParts[2]), 'I'),
            default => substr_count($romanNumeralParts[2], 'I'),
        };

        return $hundreds + $tens + $ones;
    }
}
