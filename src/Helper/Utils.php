<?php
declare(strict_types=1);
/**
 * Provides some useful methods like i.e. debug-function
 *   - Use class-methods suggested by IDE-Code-Completion
 *
 * @class Utils
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2021 Oliver Noth
 */

namespace OliverNoth\ArabicRomanNumberConverter\Helper;

/**
 * Class Utils
 *
 * @package OliverNoth\ArabicRomanNumberConverter\Helper
 * @since 1.0.0
 */
final class Utils
{
    /**
     * Wrapper for var_dump used as helper method to print out passed variable.
     *
     * @codeCoverageIgnore
     * @param mixed $var Variable to print out
     * @param bool $toArray Whether to print out an object as an array or in object manner (defaults to true)
     * @param bool $showFrom Whether to print out file and line, where debug call has occurred (defaults to true)
     * @since 1.0.0
     */
    public static function debug(mixed $var = false, bool $toArray = true, bool $showFrom = true)
    {
        // Buffer certain ini-settings
        $htmlErrors = ini_get('html_errors');

        // Ensure error html formatting
        ini_set('html_errors', '1');

        if (false !== ($xdebug = function_exists('xdebug_get_code_coverage'))) {
            $xdebugMaxDepth = ini_get('xdebug.var_display_max_depth');
            $xdebugMaxChildren = ini_get('xdebug.var_display_max_children');
            $xdebugMaxData = ini_get('xdebug.var_display_max_data');

            // Ensure non-restricted var displaying
            ini_set('xdebug.var_display_max_depth', '100');
            ini_set('xdebug.var_display_max_children', '2048');
            ini_set('xdebug.var_display_max_data', '-1');
        }

        if (false === $xdebug) {
            echo '<pre>';
        }

        if ($showFrom) {
            $backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS);

            echo(
                '<span style="font-size: 7pt; display: block;">' .
                '<strong>' . substr($backtrace[1]['file'], 1) . '</strong>' .
                ' (line <strong>' . $backtrace[1]['line'] . '</strong>)' .
                '</span>'
            );
        }

        if (true === $toArray && is_object($var)) {
            $var = (array)$var;
        }

        ob_start();
        @var_dump($var);
        $var = preg_replace('/' . addcslashes(__FILE__, '/') . ':\d+:/', '', ob_get_clean());

        echo $var;

        if (false === $xdebug) {
            echo '</pre>';
        }

        // Restore buffered ini-settings
        ini_set('html_errors', $htmlErrors);

        if (false !== $xdebug) {
            ini_set('xdebug.var_display_max_depth', $xdebugMaxDepth);
            ini_set('xdebug.var_display_max_children', $xdebugMaxChildren);
            ini_set('xdebug.var_display_max_data', $xdebugMaxData);
        }
    }

    /**
     * Gets an excerpt of a given string.
     *
     * If the string is not longer than the given excerpt length, the non-processed string will be returned.
     *
     * Sample return:
     * ```
     * Utils::excerpt('Lorem ipsum dolor sit amet', 20, '...') will return 'Lorem ipsum dolor...'
     * ```
     *
     * @param string $string The string to get the excerpt from
     * @param int $length Excerpt length
     * @param string $trailing Will be appended to the excerpt
     * @return string
     * @since 1.0.0
     */
    public static function excerpt(?string $string, int $length = 50, string $trailing = '...'): string
    {
        $string ??= 'null';

        if (strlen($string) > $length) {
            $string = preg_replace('/[\t\r\n ]+/', ' ', $string);

            return substr($string, 0, $length - strlen($trailing)) . $trailing;
        }

        return $string;
    }
}