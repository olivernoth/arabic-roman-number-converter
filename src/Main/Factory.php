<?php
declare(strict_types=1);
/**
 * Entry point to ArabicRomanNumberConverter
 *   - Acts as a factory: Creates ArabicRomanNumberConverter for a given numeral
 *   - Use class-methods suggested by IDE-Code-Completion
 *
 * @class Factory
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2021 Oliver Noth
 */

namespace OliverNoth\ArabicRomanNumberConverter\Main;

use \OliverNoth\ArabicRomanNumberConverter\Converter\ArabicRomanNumberConverter as NumberConverter;

/**
 * Class Factory
 *
 * @package OliverNoth\ArabicRomanNumberConverter
 * @since 1.0.0
 */
class Factory
{
    /**
     * Creates a new instance of \OliverNoth\ArabicRomanNumberConverter\Converter\ArabicRomanNumberConverter,
     * which then can be used to convert given numeral and get the according conversion.
     *
     * @param ?string $maybeNumeral Numeral to get the according conversion from
     * @return NumberConverter
     */
    public function createNumberConverter(?string $maybeNumeral): NumberConverter
    {
        return new NumberConverter($maybeNumeral);
    }
}