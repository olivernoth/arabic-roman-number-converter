<?php
declare(strict_types=1);
/**
 * Test case for OliverNoth\ArabicRomanNumberConverter\Converter\ArabicRomanNumberConverter
 *   - Contains methods for testing the behaviour of the tested class
 *
 * @class NumberConverterTest
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2021 Oliver Noth
 */

namespace OliverNoth\ArabicRomanNumberConverter\Tests\Converter;

use \PHPUnit\Framework\TestCase;
use OliverNoth\ArabicRomanNumberConverter\Converter\ArabicRomanNumberConverter as NumberConverter;

/**
 * Class NumberConverterTest
 *
 * @coversDefaultClass \OliverNoth\ArabicRomanNumberConverter\Converter\ArabicRomanNumberConverter
 * @group converter
 * @package OliverNoth\ArabicRomanNumberConverter\Tests
 * @since 1.0.0
 */
final class NumberConverterTest extends TestCase
{
    /**
     * Provides candidate data for testing:
     * - NumberConverter::__construct()
     * - NumberConverter::setCandidate()
     * - NumberConverter::getCandidate()
     *
     * @return array[]
     * @see \OliverNoth\ArabicRomanNumberConverter\Tests\Converter\NumberConverterTest::testConstruct()
     * @see \OliverNoth\ArabicRomanNumberConverter\Tests\Converter\NumberConverterTest::testSetCandidate()
     * @since 1.0.0
     */
    public function candidateProvider(): array
    {
        return [
            [null, NumberConverter::class],
            ['', NumberConverter::class],
            [1968, NumberConverter::class],
            ['1968', NumberConverter::class],
            ['MCMLXVIII', NumberConverter::class],
            ['mcmlxviii', NumberConverter::class],
        ];
    }

    /**
     * Provides candidates and its expected conversion results for testing:
     * - NumberConverter::convert()
     * - NumberConverter::getConversionNotes()
     *
     * @return array[]
     * @see \OliverNoth\ArabicRomanNumberConverter\Tests\Converter\NumberConverterTest::testConvert()
     * @since 1.0.0
     */
    public function conversionProvider(): array
    {
        return [
            [null, null, ["Failed! An empty string cannot be converted.", "Given '' could not be converted."]],
            ['', '', ["Failed! An empty string cannot be converted.", "Given '' could not be converted."]],
            [1968, 'MCMLXVIII', []],
            ['1968', 'MCMLXVIII', ["Arabic numeral '1968' has been converted to Roman numeral: 'MCMLXVIII'."]],
            ['MCMLXVIII', 1968, ["Roman numeral 'MCMLXVIII' has been converted to Arabic numeral: '1968'."]],
            ['mcmlxviii', 'mcmlxviii', ["Failed! Given 'mcmlxviii' is neither an arabic number nor a roman numeral.", "Given 'mcmlxviii' could not be converted."]],
            ['foobar', 'foobar', ["Failed! Given 'foobar' is neither an arabic number nor a roman numeral.", "Given 'foobar' could not be converted."]],
        ];
    }

    /**
     * Provides candidates and its expected determination results for testing:
     * - NumberConverter::determineType()
     * - NumberConverter::getConversionNotes()
     *
     * @return array[]
     * @see \OliverNoth\ArabicRomanNumberConverter\Tests\Converter\NumberConverterTest::testDetermineType()
     * @since 1.0.0
     */
    public function determineTypeProvider(): array
    {
        return [
            [null,
                [
                    'return' => null,
                    'type' => null,
                    'conversionNotes' => ['Failed! An empty string cannot be converted.'],
                ],
            ],
            ['',
                [
                    'return' => null,
                    'type' => null,
                    'conversionNotes' => ['Failed! An empty string cannot be converted.'],
                ],
            ],
            [1968, []],
            ['1968',
                [
                    'return' => ['1968'],
                    'type' => 'arabic',
                    'conversionNotes' => [],
                ],
            ],
            ['MCMLXVIII',
                [
                    'return' => ['MCM', 'LX', 'VIII'],
                    'type' => 'roman',
                    'conversionNotes' => [],
                ],
            ],
            ['mcmlxviii',
                [
                    'return' => null,
                    'type' => null,
                    'conversionNotes' => ["Failed! Given 'mcmlxviii' is neither an arabic number nor a roman numeral."],
                ],
            ],
            ['foobar',
                [
                    'return' => null,
                    'type' => null,
                    'conversionNotes' => ["Failed! Given 'foobar' is neither an arabic number nor a roman numeral."],
                ],
            ],
        ];
    }

    /**
     * Provides candidates and its expected validation results for testing:
     * - NumberConverter::validate()
     * - NumberConverter::getConversionNotes()
     *
     * @return array[]
     * @see \OliverNoth\ArabicRomanNumberConverter\Tests\Converter\NumberConverterTest::testValidate()
     * @since 1.0.0
     */
    public function validationProvider(): array
    {
        return [
            [null,
                [
                    'return' => null,
                    'conversionNotes' => ['Failed! An empty string cannot be converted.'],
                ],
            ],
            ['',
                [
                    'return' => null,
                    'conversionNotes' => ['Failed! An empty string cannot be converted.'],
                ],
            ],
            [1968, []],
            ['1968',
                [
                    'return' => ['arabic' => ['1968']],
                    'conversionNotes' => [],
                ],
            ],
            ['MCMLXVIII',
                [
                    'return' => ['roman' => ['MCM', 'LX', 'VIII']],
                    'conversionNotes' => [],
                ],
            ],
            ['mcmlxviii',
                [
                    'return' => null,
                    'conversionNotes' => ["Failed! Given 'mcmlxviii' is neither an arabic number nor a roman numeral."],
                ],
            ],
            ['foobar',
                [
                    'return' => null,
                    'conversionNotes' => ["Failed! Given 'foobar' is neither an arabic number nor a roman numeral."],
                ],
            ],
        ];
    }

    /**
     * Provides candidates and its expected validation results for testing:
     * - NumberConverter::validateRomanNumeral()
     *
     * @return array[]
     * @see \OliverNoth\ArabicRomanNumberConverter\Tests\Converter\NumberConverterTest::testValidateRomanNumeral()
     * @since 1.0.0
     */
    public function validationRomanNumeralsProvider(): array
    {
        return [
            [null,
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
            ['',
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
            [1968,
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
            ['1968',
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
            ['MCMLXVIII',
                [
                    'return' => ['roman' => ['MCM', 'LX', 'VIII']],
                ],
            ],
            ['mcmlxviii',
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
            ['foobar',
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
        ];
    }

    /**
     * Provides candidates and its expected validation results for testing:
     * - NumberConverter::validateArabicNumeral()
     *
     * @return array[]
     * @see \OliverNoth\ArabicRomanNumberConverter\Tests\Converter\NumberConverterTest::testValidateArabicNumeral()
     * @since 1.0.0
     */
    public function validationArabicNumeralsProvider(): array
    {
        return [
            [null,
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
            ['',
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
            [1968,
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
            ['1968',
                [
                    'return' => ['arabic' => ['1968']],
                ],
            ],
            ['MCMLXVIII',
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
            ['mcmlxviii',
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
            ['foobar',
                [
                    'throwsInvalidArgumentException' => true,
                    'return' => [],
                ],
            ],
        ];
    }

    /**
     * Provides candidates and its expected conversion results for testing:
     * - NumberConverter::convertArabic()
     *
     * @return array[]
     * @see \OliverNoth\ArabicRomanNumberConverter\Tests\Converter\NumberConverterTest::testConvertArabic()
     * @since 1.0.0
     */
    public function convertArabicNumeralsProvider(): array
    {
        return [
            [null,
                [
                    'throwsTypeError' => true,
                    'return' => '',
                ],
            ],
            ['',
                [
                    'throwsTypeError' => true,
                    'return' => '',
                ],
            ],
            [1968,
                [
                    'return' => 'MCMLXVIII',
                ],
            ],
            ['1968',
                [
                    'return' => 'MCMLXVIII',
                ],
            ],
            ['MCMLXVIII',
                [
                    'throwsTypeError' => true,
                    'return' => '',
                ],
            ],
            ['mcmlxviii',
                [
                    'throwsTypeError' => true,
                    'return' => '',
                ],
            ],
            ['foobar',
                [
                    'throwsTypeError' => true,
                    'return' => '',
                ],
            ],
        ];
    }

    /**
     * Provides candidates and its expected conversion results for testing:
     * - NumberConverter::convertRoman()
     *
     * @return array[]
     * @see \OliverNoth\ArabicRomanNumberConverter\Tests\Converter\NumberConverterTest::testConvertRoman()
     * @since 1.0.0
     */
    public function convertRomanNumeralsProvider(): array
    {
        return [
            [null,
                [
                    'throwsTypeError' => true,
                    'return' => '',
                ],
            ],
            ['',
                [
                    'throwsTypeError' => true,
                    'return' => '',
                ],
            ],
            [1968,
                [
                    'throwsTypeError' => true,
                    'return' => '',
                ],
            ],
            ['1968',
                [
                    'throwsTypeError' => true,
                    'return' => '',
                ],
            ],
            [['MCM', 'LX', 'VIII'],
                [
                    'return' => 1968,
                ],
            ],
            [['MCD', 'XC', 'IX'],
                [
                    'return' => 1499,
                ],
            ],
            [['MD', 'XL', 'IV'],
                [
                    'return' => 1544,
                ],
            ],
            [['M', 'L', 'III'],
                [
                    'return' => 1053,
                ],
            ],
            [['M', 'X', 'III'],
                [
                    'return' => 1013,
                ],
            ],
            ['mcmlxviii',
                [
                    'throwsTypeError' => true,
                    'return' => '',
                ],
            ],
            ['foobar',
                [
                    'throwsTypeError' => true,
                    'return' => '',
                ],
            ],
        ];
    }

    /**
     * Tests:
     * - NumberConverter::__construct()
     *
     * @dataProvider candidateProvider
     * @covers ::__construct
     * @param mixed $param
     * @param string $expected
     * @return void
     * @since 1.0.0
     */
    public function testConstruct(mixed $param, string $expected): void
    {
        if (is_int($param)) {
            $this->expectException(\TypeError::class);
        }

        $this->assertInstanceOf($expected, new NumberConverter($param));
    }

    /**
     * Tests:
     * - NumberConverter::setCandidate()
     * - NumberConverter::getCandidate()
     *
     * @dataProvider candidateProvider
     * @covers ::setCandidate
     * @covers ::getCandidate
     * @param mixed $param
     * @param string $expected
     * @return void
     * @since 1.0.0
     */
    public function testSetCandidate(mixed $param, string $expected): void
    {
        if (is_int($param)) {
            $this->expectException(\TypeError::class);
        }

        $numberConverter = (new NumberConverter())->setCandidate($param);

        $this->assertInstanceOf($expected, $numberConverter);
        $this->assertSame($param, $numberConverter->getCandidate());
    }

    /**
     * Tests:
     * - NumberConverter::convert()
     *
     * @dataProvider conversionProvider
     * @covers ::convert
     * @covers ::getConversionNotes
     * @param mixed $param
     * @param mixed $expected
     * @param array $conversionNotes
     * @return void
     * @since 1.0.0
     */
    public function testConvert(mixed $param, mixed $expected, array $conversionNotes): void
    {
        if (is_int($param)) {
            $this->expectException(\TypeError::class);
        }

        /**
         * 1st variant: Pass param via constructor
         */
        $constructedNumberConverter = new NumberConverter($param);
        $this->assertSame($expected, $constructedNumberConverter->convert());
        $this->assertSame($conversionNotes, $constructedNumberConverter->getConversionNotes());

        /**
         * 2nd variant: Pass param via setter
         */
        $setNumberConverter = (new NumberConverter())->setCandidate($param);
        $this->assertSame($expected, $setNumberConverter->convert());
        $this->assertSame($conversionNotes, $setNumberConverter->getConversionNotes());

        /**
         * 3rd variant: Pass param via convert method
         */
        $pureNumberConverter = new NumberConverter();
        $this->assertSame($expected, $pureNumberConverter->convert($param));
        $this->assertSame($conversionNotes, $pureNumberConverter->getConversionNotes());
    }

    /**
     * Tests:
     * - NumberConverter::determineType()
     * - NumberConverter::getConversionNotes()
     *
     * @dataProvider determineTypeProvider
     * @covers ::determineType
     * @covers ::getConversionNotes
     * @param mixed $param
     * @param array $expected
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testDetermineType(mixed $param, array $expected): void
    {
        if (is_int($param)) {
            $this->expectException(\TypeError::class);
        }

        $converter = new NumberConverter($param);

        $reflection = new \ReflectionClass($converter);

        $protectedMethodDetermineType = $reflection->getMethod('determineType');
        $protectedMethodDetermineType->setAccessible(true);

        $protectedPropertyType = $reflection->getProperty('type');
        $protectedPropertyType->setAccessible(true);

        $return = $protectedMethodDetermineType->invoke($converter, $param);

        $this->assertSame($expected['return'], $return);
        $this->assertSame($expected['type'], $protectedPropertyType->getValue($converter));
        $this->assertSame($expected['conversionNotes'], $converter->getConversionNotes());
    }

    /**
     * Tests:
     * - NumberConverter::validate()
     * - NumberConverter::getConversionNotes()
     *
     * @dataProvider validationProvider
     * @covers ::validate
     * @covers ::getConversionNotes
     * @param mixed $param
     * @param array $expected
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testValidate(mixed $param, array $expected): void
    {
        if (is_int($param)) {
            $this->expectException(\TypeError::class);
        }

        $converter = new NumberConverter($param);

        $this->assertSame($expected['return'], $converter->validate());
        $this->assertSame($expected['conversionNotes'], $converter->getConversionNotes());
    }

    /**
     * Tests:
     * - NumberConverter::validateRomanNumeral()
     *
     * @dataProvider validationRomanNumeralsProvider
     * @covers ::validateRomanNumeral
     * @param mixed $param
     * @param array $expected
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testValidateRomanNumeral(mixed $param, array $expected): void
    {
        if (is_int($param)) {
            $this->expectException(\TypeError::class);
        }

        $converter = new NumberConverter($param);

        $reflection = new \ReflectionClass($converter);

        $protectedMethodValidateRomanNumeral = $reflection->getMethod('validateRomanNumeral');
        $protectedMethodValidateRomanNumeral->setAccessible(true);

        if (false !== ($expected['throwsInvalidArgumentException'] ?? false)) {
            $this->expectException(\InvalidArgumentException::class);
        }

        $this->assertSame($expected['return'], $protectedMethodValidateRomanNumeral->invoke($converter, $param));
    }

    /**
     * Tests:
     * - NumberConverter::validateArabicNumeral()
     *
     * @dataProvider validationArabicNumeralsProvider
     * @covers ::validateArabicNumeral
     * @param mixed $param
     * @param array $expected
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testValidateArabicNumeral(mixed $param, array $expected): void
    {
        if (is_int($param)) {
            $this->expectException(\TypeError::class);
        }

        $converter = new NumberConverter($param);

        $reflection = new \ReflectionClass($converter);

        $protectedMethodValidateArabicNumeral = $reflection->getMethod('validateArabicNumeral');
        $protectedMethodValidateArabicNumeral->setAccessible(true);

        if (false !== ($expected['throwsInvalidArgumentException'] ?? false)) {
            $this->expectException(\InvalidArgumentException::class);
        }

        $this->assertSame($expected['return'], $protectedMethodValidateArabicNumeral->invoke($converter, $param));
    }

    /**
     * Tests:
     * - NumberConverter::convertArabic()
     *
     * @dataProvider convertArabicNumeralsProvider
     * @covers ::convertArabic
     * @param mixed $param
     * @param array $expected
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testConvertArabic(mixed $param, array $expected): void
    {
        $converter = new NumberConverter();

        $reflection = new \ReflectionClass($converter);

        $protectedMethodConvertArabic = $reflection->getMethod('convertArabic');
        $protectedMethodConvertArabic->setAccessible(true);

        if (false !== ($expected['throwsTypeError'] ?? false)) {
            $this->expectException(\TypeError::class);
        }

        $this->assertSame($expected['return'], $protectedMethodConvertArabic->invoke($converter, $param));
    }

    /**
     * Tests:
     * - NumberConverter::convertRoman()
     *
     * @dataProvider convertRomanNumeralsProvider
     * @covers ::convertRoman
     * @param mixed $param
     * @param array $expected
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testConvertRoman(mixed $param, array $expected): void
    {
        $converter = new NumberConverter();

        $reflection = new \ReflectionClass($converter);

        $protectedMethodConvertRoman = $reflection->getMethod('convertRoman');
        $protectedMethodConvertRoman->setAccessible(true);

        if (false !== ($expected['throwsTypeError'] ?? false)) {
            $this->expectException(\TypeError::class);
        }

        $this->assertSame($expected['return'], $protectedMethodConvertRoman->invoke($converter, $param));
    }
}