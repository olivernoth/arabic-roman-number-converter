<?php
declare(strict_types=1);
/**
 * Test case for OliverNoth\ArabicRomanNumberConverter\Helper\Utils
 *   - Contains methods for testing the behaviour of the tested class
 *
 * @class UtilsTest
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2021 Oliver Noth
 */

namespace OliverNoth\ArabicRomanNumberConverter\Tests\Helper;

use \PHPUnit\Framework\TestCase;
use \OliverNoth\ArabicRomanNumberConverter\Helper\Utils;

/**
 * Class UtilsTest
 *
 * @coversDefaultClass \OliverNoth\ArabicRomanNumberConverter\Helper\Utils
 * @group utils
 * @package OliverNoth\ArabicRomanNumberConverter\Tests\Helper
 * @since 1.0.0
 */
final class UtilsTest extends TestCase
{
    /**
     * Provides data for testing Utils::excerpt().
     *
     * @return array
     * @see \OliverNoth\ArabicRomanNumberConverter\Tests\Helper\UtilsTest::testExcerpt()
     * @since 1.0.0
     */
    public function excerptParamProvider(): array
    {
        return [
            ['', ''],
            [null, 'null'],
            ['foobar', 'foobar'],
            [
                'Parturient dis semper taciti interdum nullam rutrum quisque, cras tellus cursus litora sem hac curae urna, tincidunt tempor consequat justo non nulla at, purus varius sociis vitae consectetur felis.',
                'Parturient dis semper taciti interdum nullam ru...',
            ],
        ];
    }

    /**
     * Tests Utils::excerpt().
     *
     * @dataProvider excerptParamProvider
     * @covers ::excerpt
     * @param ?string $param
     * @param string $expected
     * @return void
     * @since 1.0.0
     */
    public function testExcerpt(?string $param, string $expected): void
    {
        $this->assertSame($expected, Utils::excerpt($param));
    }
}