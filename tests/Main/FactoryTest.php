<?php
declare(strict_types=1);
/**
 * Test case for OliverNoth\ArabicRomanNumberConverter\Factory
 *   - Contains methods for testing the behaviour of the tested class
 *
 * @class FactoryTest
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2021 Oliver Noth
 */

namespace OliverNoth\ArabicRomanNumberConverter\Tests\Main;

use \PHPUnit\Framework\TestCase;
use OliverNoth\ArabicRomanNumberConverter\Main\Factory;
use OliverNoth\ArabicRomanNumberConverter\Converter\ArabicRomanNumberConverter as NumberConverter;

/**
 * Class FactoryTest
 *
 * @coversDefaultClass \OliverNoth\ArabicRomanNumberConverter\Main\Factory
 * @group factory
 * @package OliverNoth\ArabicRomanNumberConverter\Tests
 * @since 1.0.0
 */
final class FactoryTest extends TestCase
{
    /**
     * Provides data for testing Factory::createNumberConverter.
     *
     * @return array[]
     * @see \OliverNoth\ArabicRomanNumberConverter\Tests\Main\FactoryTest::testCreateArabicRomanNumberConverter()
     * @since 1.0.0
     */
    public function createArabicRomanNumberConverterProvider(): array
    {
        return [
            ['', NumberConverter::class],
            [1968, NumberConverter::class],
            ['1968', NumberConverter::class],
            ['MCMLXVIII', NumberConverter::class],
            [null, NumberConverter::class],
        ];
    }

    /**
     * Tests, if a new Factory-Instance can create a new NumberConverter.
     *
     * @dataProvider createArabicRomanNumberConverterProvider
     * @covers ::createNumberConverter
     * @param mixed $param
     * @param string $expected
     * @return void
     * @since 1.0.0
     */
    public function testCreateArabicRomanNumberConverter(mixed $param, string $expected): void
    {
        if (is_int($param)) {
            $this->expectException(\TypeError::class);
        }

        $this->assertInstanceOf($expected, (new Factory())->createNumberConverter($param));
    }
}