<?php
declare(strict_types=1);
/*
 * This file is part of arabic-roman-number-converter/tests.
 * It acts as phpunit bootstrap file.
 *
 * (c) Oliver Noth <info@nothbetrieb.de>
 */

// Class loader
require_once __DIR__ . '/../vendor/autoload.php';