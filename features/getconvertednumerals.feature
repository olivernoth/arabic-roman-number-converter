Feature: Get converted numeral from an arbitrary roman or arabic numeral
  In order to use a converted arbitrary roman or arabic numeral
  As a tester
  I need to be able to receive the opposite numeral

  Rules:
  - numeral is well-formed

  Scenario: numeral is roman
    Given there is an arbitrary numeral "MCMLXVIII"
    When I convert the arbitrary numeral "MCMLXVIII"
    Then I should get the converted numeral "1968"

  Scenario: numeral is arabic
    Given there is an arbitrary numeral "1968"
    When I convert the arbitrary numeral "1968"
    Then I should get the converted numeral "MCMLXVIII"
