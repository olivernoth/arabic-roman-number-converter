<?php
declare(strict_types=1);

/**
 * Behat test context for OliverNoth\ArabicRomanNumberConverter\Main\Factory and OliverNoth\ArabicRomanNumberConverter\Converter\ArabicRomanNumberConverter
 *   - Defines application features from the specific context
 *
 * @class FeatureContext
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2021 Oliver Noth
 */

use Behat\Behat\Context\Context;
use OliverNoth\ArabicRomanNumberConverter\{Main\Factory as NumberConverterFactory,
    Converter\ArabicRomanNumberConverter as NumberConverter
};
use PHPUnit\Framework\Assert as PHPUnitAssert;

/**
 * Class FeatureContext
 *
 * @since 1.0.0
 */
class FeatureContext implements Context
{
    /**
     * Holds number converter factory, which can create a number converter distance.
     *
     * @var ?NumberConverterFactory
     * @since 1.0.0
     */
    private ?NumberConverterFactory $numberConverterFactory = null;

    /**
     * Holds number converter, which converts an arbitrary roman or arabic numeral.
     *
     * @var ?NumberConverter
     * @since 1.0.0
     */
    private ?NumberConverter $numberConverter = null;

    /**
     * Initializes context.
     *
     * @since 1.0.0
     */
    public function __construct()
    {
        $this->numberConverterFactory = new NumberConverterFactory();
    }

    /**
     * @Given there is an arbitrary numeral :arg1
     * @param string $arg1
     * @since 1.0.0
     */
    public function thereIsAnArbitraryNumeral(string $arg1): void
    {
        $this->numberConverter = $this->numberConverterFactory->createNumberConverter($arg1);

        PHPUnitAssert::assertInstanceOf(NumberConverter::class, $this->numberConverter);
    }

    /**
     * @When I convert the arbitrary numeral :arg1
     * @param string $arg1
     * @since 1.0.0
     * @throws ReflectionException
     */
    public function iConvertTheArbitraryNumeral(string $arg1): void
    {
        $reflection = new ReflectionClass($this->numberConverter);
        $candidateProperty = $reflection->getProperty('candidate');
        $candidateProperty->setAccessible(true);

        PHPUnitAssert::assertSame($arg1, $candidateProperty->getValue($this->numberConverter));
    }

    /**
     * @Then I should get the converted numeral :arg1
     * @param string $arg1
     * @since 1.0.0
     */
    public function iShouldGetTheConvertedNumeral(string $arg1): void
    {
        $converted = $this->numberConverter->convert();

        PHPUnitAssert::assertEquals($arg1, $converted);
    }
}
