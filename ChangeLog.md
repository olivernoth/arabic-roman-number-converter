# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2021-11-01
### Changed
- Minor readjustmens in README.md and .gitlab-ci.yml

## [1.0.1] - 2021-10-30
### Fixed
- .gitlab-ci.yml: Fixed invoking unittest

## [1.0.0] - 2021-10-27
- Initial release

[1.0.2]: https://gitlab.com/olivernoth/arabic-roman-number-converter/-/tags/v1.0.2
[1.0.1]: https://gitlab.com/olivernoth/arabic-roman-number-converter/-/tags/v1.0.1
[1.0.0]: https://gitlab.com/olivernoth/arabic-roman-number-converter/-/tags/v1.0.0
